import React from 'react';
import './TaskItems.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function TaskItems(props){
    const items = props.items;
    const taskItems = items.map(item =>
        {
            return <div className="task" key={item.key}>
                <p>
                <span>
                    <FontAwesomeIcon className="ticons"icon='check'
                    onClick={() => props.deleteItem(item.key)}/>    
                </span>
                
                {item.text}
                </p>
            </div>
        })
    return(
       <div>{taskItems}</div>
        
    )
}
export default TaskItems;